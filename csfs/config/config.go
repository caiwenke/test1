package config

import (
	"path/filepath"
	"os"
	"io/ioutil"
	"encoding/json"
	"path"
	"log"
)

// Convert JSON to Go struct
// https://mholt.github.io/json-to-go/
type Config struct {
	User string `json:"user"`
	Key  string `json:"key"`
	Servers []struct {
		IP   string `json:"ip"`
		Port string `json:"port"`
		//Available     bool      `json:"available"`
		//LastCheckDate time.Time `json:"last_check_date"`
	} `json:"servers"`
	HashRingNodeReplicaNum   int `json:"hash_ring_node_replica_num"`
	TransferServerReplicaNum int `json:"transfer_server_replica_num"`
	//PartSizeLimit            int `json:"part_size_limit"`
	//UploadThreadPerFileLimit int `json:"upload_thread_per_file_limit"`
	UID                      int `json:"uid"`
	Gid                      int `json:"gid"`
	ReqTimeout               *int `json:"request_timeout"`
}

func ParseConfig() (*Config, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return nil, err
	}
	bin, err := ioutil.ReadFile(path.Join(dir, "config.json"))
	if err != nil {
		return nil, err
	}
	var cfg Config
	err = json.Unmarshal(bin, &cfg)
	if err != nil {
		return nil, err
	}
	if len(cfg.Servers) < 1 {
		log.Fatal("Servers are not found in the config file")
	}
	return &cfg, nil
}
