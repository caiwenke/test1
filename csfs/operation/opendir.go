package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newOpenDirReq(path *string) *mds.Req {
	limit := uint64(10000)
	page := uint64(1)
	user := myutil.GLOBAL.Username()
	key := myutil.GLOBAL.UserKey()
	// todo
	req := &mds.Req{
		RawReq: &mds.RawReq{
			Req: &mds.RawReq_ReaddirReq{
				ReaddirReq: &mds.ReadDirReq{
					Path:  path,
					Limit: &limit,
					Page:  &page,
				},
			},
		},
		UserName: &user,
		UserKey:  &key,
	}
	return req
}

func OpenDir(path string) (*mds.DirList, error) {

	req := newOpenDirReq(&path)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return nil, err
	}

	var readdirRes *mds.ReadDirRes
	switch x := res.Res.(type) {
	case *mds.Res_ReaddirRes:
		readdirRes = x.ReaddirRes
	default:
		return nil, myutil.NewError(myutil.ResponseTypeMissMatch, err)
	}
	var dirList *mds.DirList
	switch x := readdirRes.Res.(type) {
	case *mds.ReadDirRes_DirList:
		dirList = x.DirList
	case *mds.ReadDirRes_Error:
		return nil, myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}

	return dirList, nil
}
