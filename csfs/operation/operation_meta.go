package operation

import (
	"csfs/pb/mds"
	"github.com/golang/protobuf/proto"
	"csfs/myutil"
)

//func OpenDir(name string, context *fuse.Context) (stream []fuse.DirEntry, code fuse.Status) {
//	data := make([]byte, 10)
//	reader := bytes.NewReader(data)
//	resp, err := http.Post("http://192.168.164.111:9999/api/rpc", "application/octet-stream", reader)
//	if
//}

func NewReadDirReq(path *string) ([]byte, error) {
	limit := uint64(10000)
	page := uint64(1)
	user := myutil.GLOBAL.Username()
	key := myutil.GLOBAL.UserKey()

	req := &mds.Req{
		RawReq: &mds.RawReq{
			Req: &mds.RawReq_ReaddirReq{
				ReaddirReq: &mds.ReadDirReq{
					Path:  path,
					Limit: &limit,
					Page:  &page,
				},
			},
		},
		UserName: &user,
		UserKey:  &key,
	}
	return proto.Marshal(req)
}
