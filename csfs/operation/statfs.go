package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newStatFsReq(path *string) *mds.Req {
	rawReq := &mds.RawReq{
		Req: &mds.RawReq_StatvfsReq{
			StatvfsReq: &mds.StatVfsReq{
				Path: path,
			},
		},
	}
	return myutil.NewReq(rawReq)
}
func StatFs(path *string) (*mds.StatVfs, error) {
	req := newStatFsReq(path)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return nil, err
	}

	var statfsRes *mds.StatVfsRes
	switch  x := res.Res.(type) {
	case *mds.Res_StatvfsRes:
		statfsRes = x.StatvfsRes
	default:
		return nil, myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}
	var statfs *mds.StatVfs
	switch  x := statfsRes.Res.(type) {
	case *mds.StatVfsRes_StatVfs:
		statfs = x.StatVfs
	case *mds.StatVfsRes_Error:
		return nil, myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return statfs, nil
}
