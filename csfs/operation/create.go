package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newCreateReq(path*string,flag *mds.CreationFlag,mode *mds.AccessMode,sessionId*string) *mds.Req{
	rawReq := &mds.RawReq{
		Req:&mds.RawReq_CreateReq{
			CreateReq:&mds.CreateReq{
				Path:path,
				Mode:mode,
				Flag:flag,
				SessionId:sessionId,
			},
		},
	}
	return myutil.NewReq(rawReq)
}


func Create(path*string,flag *mds.CreationFlag,mode *mds.AccessMode,sessionId string) error{
	req := newCreateReq(path,flag,mode,&sessionId)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var creatRes *mds.CreateRes
	switch  x := res.Res.(type) {
	case *mds.Res_CreateRes:
		creatRes = x.CreateRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := creatRes.Res.(type) {

	case *mds.CreateRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}