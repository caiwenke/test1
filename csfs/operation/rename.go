package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newRenameReq(old *string,new *string) *mds.Req{
	rawReq:= &mds.RawReq{
		Req:&mds.RawReq_RenameReq{
			RenameReq:&mds.RenameReq{
				Src:old,
				Dest:new,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func Rename(old *string,new *string) error{
	req := newRenameReq(old, new)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var renameRes *mds.RenameRes
	switch  x := res.Res.(type) {
	case *mds.Res_RenameRes:
		renameRes = x.RenameRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := renameRes.Res.(type) {

	case *mds.RenameRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}