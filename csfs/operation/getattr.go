package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newGetAttrReq(path *string) *mds.Req {
	//req := &mds.Req{
	//	RawReq: &mds.RawReq{
	//		Req: &mds.RawReq_GetattrReq{
	//			GetattrReq: &mds.GetAttrReq{
	//				Path: path,
	//			},
	//		},
	//	},
	//}
	rawReq := &mds.RawReq{
		Req: &mds.RawReq_GetattrReq{
			GetattrReq: &mds.GetAttrReq{
				Path: path,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func GetAttr(path *string) (*mds.Stat, error) {
	req := newGetAttrReq(path)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return nil, err
	}

	var getAttrRes *mds.GetAttrRes
	switch  x := res.Res.(type) {
	case *mds.Res_GetattrRes:
		getAttrRes = x.GetattrRes
	default:
		return nil, myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}
	var stat *mds.Stat
	switch  x := getAttrRes.Res.(type) {
	case *mds.GetAttrRes_Stat:
		stat = x.Stat
	case *mds.GetAttrRes_Error:
		return nil, myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return stat, nil
}
