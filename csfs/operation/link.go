package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newLinkReq(old *string, new *string) *mds.Req {
	rawReq := &mds.RawReq{
		Req: &mds.RawReq_LinkReq{
			LinkReq: &mds.LinkReq{
				ExistingPath: old,
				NewPath:      new,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func Link(old *string, new *string) error {
	req := newLinkReq(old, new)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var linkRes *mds.LinkRes
	switch  x := res.Res.(type) {
	case *mds.Res_LinkRes:
		linkRes = x.LinkRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := linkRes.Res.(type) {
	case *mds.LinkRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}
