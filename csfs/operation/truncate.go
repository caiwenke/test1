package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
	"fmt"
	"reflect"
)

func newTruncateReq	(path *string, size uint64) * mds.Req{
	rawReq := &mds.RawReq{
		Req:&mds.RawReq_TruncateReq{
			TruncateReq:&mds.TruncateReq{
				Path:path,
				Size:&size,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func Truncate(path *string, size uint64)  error{
	req := newTruncateReq(path, size)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var truncateRes *mds.TruncateRes
	switch  x := res.Res.(type) {
	case *mds.Res_TruncateRes:
		truncateRes = x.TruncateRes
	default:
		info := fmt.Sprintf("Response type miss match: %v, type: %v",x,reflect.TypeOf(res.Res))
		return myutil.NewError1(myutil.ResponseTypeMissMatch,info, nil)
	}

	switch  x := truncateRes.Res.(type) {

	case *mds.TruncateRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}
