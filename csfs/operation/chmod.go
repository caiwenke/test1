package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newChmodReq(path *string, mode *mds.StMode) *mds.Req {
	rawReq := &mds.RawReq{
		Req: &mds.RawReq_ChmodReq{
			ChmodReq: &mds.ChmodReq{
				Path:   path,
				StMode: mode,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func Chmod(path *string, mode *mds.StMode) error {
	stat, err := GetAttr(path)
	if err != nil {
		return err
	}
	// There is no type info in mode
	mode.Type = stat.StMode.Type
	return chmod1(path, mode)
}

func chmod1(path *string, mode *mds.StMode) error {
	req := newChmodReq(path, mode)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var chmodRes *mds.ChmodRes
	switch  x := res.Res.(type) {
	case *mds.Res_ChmodRes:
		chmodRes = x.ChmodRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := chmodRes.Res.(type) {

	case *mds.ChmodRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}
