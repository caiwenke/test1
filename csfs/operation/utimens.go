package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newUtimensReq(path*string,atime uint64,mtime uint64) *mds.Req{
	rawReq:= &mds.RawReq{
		Req:&mds.RawReq_UtimensReq{
			UtimensReq:&mds.UtimensReq{
				Path:path,
				Atime:&atime,
				Mtime:&mtime,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func Utimens(path*string,atime uint64,mtime uint64) error{
	req := newUtimensReq(path, atime,mtime)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var utimensRes *mds.UtimensRes
	switch  x := res.Res.(type) {
	case *mds.Res_UtimensRes:
		utimensRes = x.UtimensRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := utimensRes.Res.(type) {

	case *mds.UtimensRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}