package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newMkdirReq(path *string) *mds.Req{
	rawReq := &mds.RawReq{
		Req:&mds.RawReq_MkdirReq{
			MkdirReq:&mds.MakeDirReq{
				Path:path,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func Mkdir(path *string) error{
	req := newMkdirReq(path)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var mkdirRes *mds.MakeDirRes
	switch  x := res.Res.(type) {
	case *mds.Res_MkdirRes:
		mkdirRes = x.MkdirRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := mkdirRes.Res.(type) {

	case *mds.MakeDirRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}