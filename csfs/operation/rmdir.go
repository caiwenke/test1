package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
	"strings"
)

func newRmdirReq(path *string) *mds.Req {
	rawReq := &mds.RawReq{
		Req: &mds.RawReq_RmdirReq{
			RmdirReq: &mds.RemoveDirReq{
				Path: path,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func Rmdir(path *string) error {
	req := newRmdirReq(path)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var rmdirRes *mds.RemoveDirRes
	switch  x := res.Res.(type) {
	case *mds.Res_RmdirRes:
		rmdirRes = x.RmdirRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := rmdirRes.Res.(type) {
	case *mds.RemoveDirRes_Error:
		info := *x.Error.Error
		if strings.ToUpper(info) == "EEXIST" {
			info = "ENOTEMPTY"
		}
		return myutil.NewError1(myutil.RequestReturnsError, info, nil)
	}
	return nil
}
