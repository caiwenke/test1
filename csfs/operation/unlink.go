package operation

import (
	"csfs/pb/mds"
	"csfs/myutil"
)

func newUnlinkReq(path * string) *mds.Req{
	rawReq :=&mds.RawReq{
		Req: &mds.RawReq_UnlinkReq{
			UnlinkReq:&mds.UnlinkReq{
				Path:path,
			},
		},
	}
	return myutil.NewReq(rawReq)
}

func Unlink(path *string) error{
	req := newUnlinkReq(path,)
	url := myutil.GLOBAL.ServerHttpUrl()
	res, err := myutil.PostReq(url, req)

	if err != nil {
		return err
	}

	var unlinkRes *mds.UnlinkRes
	switch  x := res.Res.(type) {
	case *mds.Res_UnlinkRes:
		unlinkRes = x.UnlinkRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := unlinkRes.Res.(type) {

	case *mds.UnlinkRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}