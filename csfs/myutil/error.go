package myutil

import (
	"github.com/hanwen/go-fuse/fuse"
	"fmt"
	"strings"
	"syscall"
)

type myError struct {
	t     ErrorType
	inner error
	text  string
}
type ErrorType int

const (
	ProtoBufError         ErrorType = iota
	HttpResponseError
	ReadResponseError
	ResponseTypeMissMatch
	RequestReturnsError
	SocketError
	ReceiveTimeout
	TransferHashringError
	ServerUnavailable
)

func NewError1(t_ ErrorType, text_ string, err error) error {
	return &myError{
		t:     t_,
		inner: err,
		text:  text_,
	}
}

func NewError(type_ ErrorType, err error) error {
	return &myError{
		t:     type_,
		inner: err,
	}
}
func (e *myError) Error() string {
	//inner := ""
	//if e.inner != nil {
	//	inner = e.inner.Error()
	//}
	//return fmt.Sprintf("text: %v, inner: %s", e, inner)
	if e.inner == nil {
		return fmt.Sprintf("info: %s", e.text)
	} else {
		return fmt.Sprintf("info: %s, inner: %s", e.text, e.inner.Error())
	}

}

func ToStatus(e error) fuse.Status {
	if e == nil {
		return fuse.OK
	}
	switch x := e.(type) {
	case *myError:
		if x.t == RequestReturnsError {
			switch  strings.ToUpper(x.text) {
			case "ENOENT":
				return fuse.ENOENT
			case "ENOTEMPTY":
				return fuse.Status(syscall.ENOTEMPTY)
			case "EEXIST":
				return fuse.Status(syscall.EEXIST)
			}
		}
	}
	return fuse.EIO
}

//func NewProtoBufError(text string, inner error) error {
//	return &protoBufError{text, inner}
//}
//
//type protoBufError struct {
//	s     string
//	inner error
//}
//
//func (e *protoBufError) Error() string {
//	return e.s
//}
//
//
////////////////////
//func NewReadResponseError(text string, inner error) error {
//	return &readResponseError{text, inner}
//}
//
//type readResponseError struct {
//	s     string
//	inner error
//}
//
//func (e *readResponseError) Error() string {
//	return e.s
//}
