package myutil

import (
	"sync"
	"time"
	"csfs/config"
	"fmt"
	"csfs/hashring"
	"encoding/binary"
	"log"
	"sync/atomic"
)

var serverUrl = ""
var GLOBAL = &Global{}
var lock = &sync.Mutex{}
var sessionId *string
var TransferRing *hashring.TransferRing
var CONFIG *config.Config
var serverUnavailable bool
var timeOffset int64

func getServerUrl(ip string, port string) string {
	return fmt.Sprintf("http://%s:%s/api/rpc", ip, port)
}

func ServerTime() uint64 {
	var serverTime = time.Now().UTC().Unix() + atomic.LoadInt64(&timeOffset)
	return uint64(serverTime)
}

func updateServerList(cfg *config.Config) error {
	var errOuter error
	for _, server := range cfg.Servers {
		url := getServerUrl(server.IP, server.Port)
		transferList, serverTime, sId, err := Heartbeat(sessionId, url)
		if err == nil {
			serverUrl = url
			TransferRing = hashring.NewTransferRing(CONFIG.HashRingNodeReplicaNum, transferList)
			sessionId = sId
			localTime := time.Now().UTC().Unix()
			delta := int64((*serverTime)/1000000) - localTime
			log.Println("[update] connected to server ", server.IP, "time offset:", delta, "second(s)")
			serverUnavailable = false
			atomic.StoreInt64(&timeOffset, delta)
			return nil
		} else {
			errOuter = err
			continue
		}
	}
	serverUnavailable = true
	info := "No available server found in the config file"
	return NewError1(ServerUnavailable, info, errOuter)
}

func InitState() error {
	cfg, err := config.ParseConfig()
	if err != nil {
		return err
	}
	CONFIG = cfg
	err = UpdateGlobalState()
	if err != nil {
		return err
	}
	return nil
}

func UpdateGlobalState() error {
	lock.Lock()
	defer lock.Unlock()
	err := updateServerList(CONFIG)
	if err != nil {
		return err
	}

	return nil
}

type Global struct {
}

func (g *Global) Username() string {
	return CONFIG.User
}

func (g *Global) UserKey() string {
	return CONFIG.Key
}

func (g *Global) ReceiveTimeout() time.Duration {
	if CONFIG.ReqTimeout == nil {
		return 60 * time.Second
	} else {
		return time.Duration(*CONFIG.ReqTimeout) * time.Second
	}
}

func (g *Global) BlockSize() uint32 {
	return 1024 * 4
}

func (g *Global) ServerHttpUrl() string {
	lock.Lock()
	defer lock.Unlock()
	return serverUrl
}

func (g *Global) TransferNode(fileId uint64) (string, error) {
	lock.Lock()
	defer lock.Unlock()
	keyBin := make([]byte, 8)
	binary.BigEndian.PutUint64(keyBin, fileId)
	nodes := TransferRing.FindNode(keyBin, CONFIG.TransferServerReplicaNum)
	if len(nodes) < 1 {
		info := fmt.Sprintf("[hashring] node not found for fileID: %v", fileId)
		return "", NewError1(TransferHashringError, info, nil)
	}
	node := nodes[0]
	transferUrl := fmt.Sprintf("%s:%s", *node.Ip, *node.Port)
	return transferUrl, nil
}

func (g *Global)FindAnotherNode(node string) (string , error) {
	lock.Lock()
	defer lock.Unlock()
	for _, v := range TransferRing.Getm(){
		if *v.Node == node {
			ip := v.Ip
			port := v.Port
			transferUrl := fmt.Sprintf("%s:%s", *ip, *port)
			return transferUrl, nil
		}
	}
	return "" , nil
}

func (g *Global) SessionID() string {
	lock.Lock()
	defer lock.Unlock()
	return *sessionId
}

func HeartBeatLoop() {
	for {
		time.Sleep(60 * time.Second)
		UpdateGlobalState()
		//runtime.GC()
	}
}
