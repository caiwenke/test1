package myutil

import (
	"bytes"
	"net/http"
	"io/ioutil"
	"csfs/pb/mds"
	"github.com/golang/protobuf/proto"
	"github.com/hanwen/go-fuse/fuse"
)

const RetryMax = 3

func PostReq(url string, req *mds.Req) (*mds.Res, error) {
	var errOuter error
	for i := 0; i < RetryMax; i++ {
		res, err := postReq1(url, req)
		if err == nil {
			return res, nil
		} else {
			UpdateGlobalState()
			errOuter = err
		}
	}
	return nil, errOuter
}

func postReq1(url string, req *mds.Req) (*mds.Res, error) {
	data, err := proto.Marshal(req)
	if err != nil {
		return nil, NewError(ProtoBufError, err)
	}

	reader := bytes.NewReader(data)

	client := http.Client{}
	httpReq, err := http.NewRequest("POST", url, reader)

	if err != nil {
		return nil, err
	}
	httpReq.Header.Add("User-Agent", "FUSE")
	httpReq.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := client.Do(httpReq)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, NewError1(HttpResponseError, resp.Status, nil)
	}
	resData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, NewError(ReadResponseError, err)
	}
	res := &mds.Res{}
	err = proto.UnmarshalMerge(resData, res)
	if err != nil {
		return nil, NewError(ProtoBufError, err)
	}
	return res, nil
}

func NewReq(rawReq *mds.RawReq) *mds.Req {
	user := GLOBAL.Username()
	key := GLOBAL.UserKey()
	req := &mds.Req{
		RawReq:   rawReq,
		UserName: &user,
		UserKey:  &key,
	}
	return req
}

func AttrMode2CsMode(mode uint32) *mds.StMode {

	var ur, uw, ux, gr, gw, gx, or, ow, ox bool
	if mode&(1<<8) > 0 {
		ur = true
	}
	if mode&(1<<7) > 0 {
		uw = true
	}
	if mode&(1<<6) > 0 {
		ux = true
	}
	if mode&(1<<5) > 0 {
		gr = true
	}
	if mode&(1<<4) > 0 {
		gw = true
	}
	if mode&(1<<3) > 0 {
		gx = true
	}
	if mode&(1<<2) > 0 {
		or = true
	}
	if mode&(1<<1) > 0 {
		ow = true
	}
	if mode&(1) > 0 {
		ox = true
	}

	res := &mds.StMode{
		U: &mds.Privil{R: &ur, W: &uw, X: &ux,},
		G: &mds.Privil{R: &gr, W: &gw, X: &gx},
		O: &mds.Privil{R: &or, W: &ow, X: &ox},
	}
	if mode&fuse.S_IFREG > 0 {
		tmp := mds.Type_file
		res.Type = &tmp
	}
	if mode&fuse.S_IFDIR > 0 {
		tmp := mds.Type_dir
		res.Type = &tmp
	}

	return res
}

func CsMode2AttrMode(mode *mds.StMode) uint32 {
	res := uint32(0)
	switch *mode.Type {
	case mds.Type_dir:
		res |= fuse.S_IFDIR
	case mds.Type_file:
		res |= fuse.S_IFREG
	}
	// User
	if *mode.U.R {
		res |= 1 << 8
	}
	if *mode.U.W {
		res |= 1 << 7
	}
	if *mode.U.X {
		res |= 1 << 6
	}
	// Group
	if *mode.G.R {
		res |= 1 << 5
	}
	if *mode.G.W {
		res |= 1 << 4
	}
	if *mode.G.X {
		res |= 1 << 3
	}
	// Other
	if *mode.O.R {
		res |= 1 << 2
	}
	if *mode.O.W {
		res |= 1 << 1
	}
	if *mode.O.X {
		res |= 1
	}
	return res
}
