package myutil

import (
	"csfs/pb/mds"
)

func newHeartbeatReq(sessionId *string) *mds.Req {
	rawReq := &mds.RawReq{
		Req: &mds.RawReq_HeartbeatReq{
			HeartbeatReq: &mds.HeartbeatReq{
				SessionId: sessionId,
			},
		},
	}
	return NewReq(rawReq)
}

func Heartbeat(sessionId *string, url string) ([]*mds.TransferServer, *uint64, *string, error) {
	req := newHeartbeatReq(sessionId)
	//url := GLOBAL.ServerHttpUrl()
	res, err := postReq1(url, req)

	if err != nil {
		return nil, nil, nil, err
	}

	var heartbeatRes *mds.HeartbeatRes
	switch  x := res.Res.(type) {
	case *mds.Res_HeartbeatRes:
		heartbeatRes = x.HeartbeatRes
	default:
		return nil, nil, nil, NewError(ResponseTypeMissMatch, nil)
	}

	return heartbeatRes.Servers, heartbeatRes.Timestamp, heartbeatRes.SessionId, nil
}
