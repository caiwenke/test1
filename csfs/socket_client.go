package main

import (
	"net"
	"encoding/binary"
	"io"
)
//
//func main() {
//	//log.Fatal("abc")
//	log.SetFlags(log.LstdFlags | log.Lshortfile)
//	path := "abc"
//	mode := pb.AccessMode_O_RDWR
//	bin, err := NewOpenReq(0, &path, true, false, mode)
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	addr := "192.168.164.111:22334"
//	conn, err := net.Dial("tcp", addr)
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	if err := send(conn, bin); err != nil {
//		log.Fatal(err)
//	}
//	res, err := read(conn)
//	if err != nil {
//		log.Fatal(err)
//	}
//	xxx := &pb.Req{}
//	proto.UnmarshalMerge(res, xxx)
//	log.Println(xxx)
//
//}

func send(conn net.Conn, data []byte) error {
	header := make([]byte, 4)
	binary.BigEndian.PutUint32(header, uint32(len(data)))

	if _, err := conn.Write(header); err != nil {
		return err
	}

	if _, err := conn.Write(data); err != nil {
		return err
	}
	return nil
}

func read(conn net.Conn) ([]byte, error) {
	header := make([]byte, 4)
	if _, err := io.ReadFull(conn, header); err != nil {
		return nil, err
	}
	length := binary.BigEndian.Uint32(header)
	data := make([]byte, length)
	if _, err := io.ReadFull(conn, data); err != nil {
		return nil, err
	}
	return data, nil
}
