// Copyright 2016 the Go-FUSE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// A Go mirror of libfuse's hello.c

package main

import (
	"flag"
	"log"

	"github.com/hanwen/go-fuse/fuse/nodefs"
	"github.com/hanwen/go-fuse/fuse/pathfs"
	"csfs/operation"
	"csfs/myutil"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile | log.Lmicroseconds)
	flag.Parse()
	if len(flag.Args()) < 1 {
		log.Fatal("Usage:\n  hello MOUNTPOINT")
	}

	//todo timeout
	err := myutil.InitState()
	if err != nil {
		log.Println("Failed to init program")
		log.Fatal(err)
	}

	go myutil.HeartBeatLoop()

	pathFsOpts := &pathfs.PathNodeFsOptions{ClientInodes: true} // enable hard link

	pathfs.NewDefaultFileSystem()
	nfs := pathfs.NewPathNodeFs(&HelloFs{}, pathFsOpts)

	server, _, err := nodefs.MountRoot(flag.Arg(0), nfs.Root(), nil)
	if err != nil {
		log.Fatalf("Mount fail: %v\n", err)
	}
	server.Serve()
}

func main1() {
	list, err := operation.OpenDir("/")
	if err != nil {
		log.Fatal(err)
	}
	log.Println(list)
}

//func main() {
//	path := "/"
//	data, err := operation.NewReadDirReq(&path)
//	if err != nil {
//		log.Fatal(err)
//	}
//	reader := bytes.NewReader(data)
//
//	client := &http.Client{}
//	req, err := http.NewRequest("POST", "http://192.168.164.111:9999/api/rpc", reader)
//	req.Header.Add("User-Agent", "FUSE")
//	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
//	resp, err := client.Do(req)
//	//resp, err := http.Post("http://192.168.164.111:9999/api/rpc", "application/x-www-form-urlencoded", reader)
//	if err != nil {
//		log.Fatal(err)
//	}
//	resData, err := ioutil.ReadAll(resp.Body)
//	if err != nil {
//		log.Fatal(err)
//	}
//	res := &mds.Res{}
//	err = proto.UnmarshalMerge(resData, res)
//	if err != nil {
//		log.Fatal(err)
//	}
//	log.Println(res)
//
//}
