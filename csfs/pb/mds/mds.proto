syntax = "proto2";

package mds;

//----------------------------------------------------------
// attributes
message FileInfo {
    required string filename = 1;
    required Stat stat = 2;
}

message Stat {
    optional int32 st_nlink = 1;
    required StMode st_mode = 2;
    required string uname = 3;
    required string gname = 4;
    required uint64 st_size = 5;
    required uint64 st_ctim = 6;
    optional uint64 st_atim = 7;
    optional uint64 st_mtim = 8;
    required uint64 st_ino = 9;
}

message StMode {
    required Type type = 1;
    required Privil u = 2;
    required Privil g = 3;
    required Privil o = 4;
}

enum Type {
    dir = 1;
    file = 2;
}

message Privil {
    required bool r = 1;
    required bool w = 2;
    required bool x = 3;
}

message CreationFlag {
    required bool O_CREAT = 1;
    required bool O_EXCL = 2;
}

enum AccessMode {
    O_RDONLY = 1;
    O_WRONLY = 2;
    O_RDWR = 3;
}

//----------------------------------------------------------
// Request
message Req {
    required string user_name = 1;
    required string user_key = 2;
    required RawReq raw_req = 3;
}

message RawReq {
    oneof req {
        GetAttrReq getattr_req = 1; // get attributes
        StatVfsReq statvfs_req = 2; // get file system stats

        MakeDirReq mkdir_req = 3;
        RemoveDirReq rmdir_req = 4;
        ReadDirReq readdir_req = 5;

        RenameReq rename_req = 6; // change name
        ChmodReq chmod_req = 7; // change mode
        ChownReq chown_req = 8; // change owner
        UtimensReq utimens_req = 9; // change the access and modification times of a file with nanosecond resolution
        TruncateReq truncate_req = 10; // change size

        CreateReq create_req = 11;

        UnlinkReq unlink_req = 12; // delete file

        HeartbeatReq heartbeat_req = 13;

        LinkReq link_req = 14;
    }
}

//----------------------------------------------------------
// Response
message Res {
    oneof res {
        GetAttrRes getattr_res = 1; // get attributes
        StatVfsRes statvfs_res = 2;

        MakeDirRes mkdir_res = 3;
        RemoveDirRes rmdir_res = 4;
        ReadDirRes readdir_res = 5;

        RenameRes rename_res = 6; // change name
        ChmodRes chmod_res = 7; // change owner
        ChownRes chown_res = 8; // change owner
        UtimensRes utimens_res = 9; // change the access and modification times of a file with nanosecond resolution
        TruncateRes truncate_res = 10; // change size

        CreateRes create_res = 11;

        UnlinkRes unlink_res = 12; // delete file

        HeartbeatRes heartbeat_res = 13;

        OtherRes other_res = 14;

        LinkRes link_res = 15;
    }
}

message ResSuccess {
    required string success = 1;
}
message ResError {
    required string error = 1;
}

//----------------------------------------------------------
// getattr
message GetAttrReq {
    required string path = 1;
}

message GetAttrRes {
    oneof res {
        Stat stat = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// get file system statistics
message StatVfsReq {
    required string path = 1;
}

message StatVfs {
    required uint64 f_bsize = 1;
    required uint64 f_frsize = 2; /* Fragment size */
    required uint64 f_blocks = 3; /* Size of fs in f_frsize units */
    required uint64 f_bfree = 4; /* Number of free blocks */
    required uint64 f_bavail = 5; /* Number of free blocks for
                                             unprivileged users */
    required uint64 f_files = 6; /* # inodes */
    required uint64 f_ffree = 7; /* # free inodes */
    required uint64 f_favail = 8; /* # free inodes for unprivileged users */
    required uint64 f_namemax = 9; /* maximum filename length */
}

message StatVfsRes {
    oneof res {
        StatVfs stat_vfs = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// mkdir
message MakeDirReq {
    required string path = 1;
}

message MakeDirRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// rmdir
message RemoveDirReq {
    required string path = 1;
}

message RemoveDirRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// readdir
message ReadDirReq {
    required string path = 1;
    required uint64 page = 2;
    required uint64 limit = 3;
}

message DirList {
    required uint64 total_count = 1;
    required uint64 current_page = 2;
    repeated FileInfo page_list = 3;
}

message ReadDirRes {
    oneof res {
        DirList dir_list = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// rename
message RenameReq {
    required string src = 1;
    required string dest = 2;
}
message RenameRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// chown
message ChownReq {
    required string path = 1;
    required string uname = 2;
    required string gname = 3;
}

message ChownRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// chmod
message ChmodReq {
    required string path = 1;
    required StMode st_mode = 2;
}

message ChmodRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// truncate
message TruncateReq {
    required string path = 1;
    required uint64 size = 2;
}

message TruncateRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// change access and modified time
message UtimensReq {
    required string path = 1;
    required uint64 atime = 2;
    required uint64 mtime = 3;
}

message UtimensRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// open
message CreateReq {
    required string path = 1;
    required CreationFlag flag = 2;
    required AccessMode mode = 3;
    required string session_id = 4;
}


message File {
    required uint64 file_id = 1;
}

message CreateRes {
    oneof res {
        File file = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// release
message ReleaseReq {
    required string path = 1;
    required string session_id = 2;
}

message ReleaseRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------

//----------------------------------------------------------
// link
message LinkReq {
    required string existing_path = 1;
    required string new_path = 2;
}

message LinkRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// unlink
message UnlinkReq {
    required string path = 1;
}

message UnlinkRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}

//----------------------------------------------------------
// getcluster
message HeartbeatReq {
    optional string session_id = 1;
}

message HeartbeatRes {
    repeated TransferServer servers = 1;
    optional string session_id = 2;
    required uint64 timestamp = 3;
}

message TransferServer {
    required string node = 1;
    required string ip = 2;
    required string port = 3;
}

//----------------------------------------------------------
// other
message OtherRes {
    oneof res {
        ResSuccess success = 1;
        ResError error = 2;
    }
}




