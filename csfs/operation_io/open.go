package operation_io

import (
	"csfs/pb/transfer"
	"github.com/golang/protobuf/proto"
	"csfs/myutil"
	"net"
	"fmt"
	"csfs/socket_util"
)

func newOpenReqBin(reqId uint64, path string) ([]byte, error) {
	mode := transfer.AccessMode_O_RDWR
	user := myutil.GLOBAL.Username()
	key := myutil.GLOBAL.UserKey()
	snapshot := "snapshot"
	req := &transfer.Req{
		RawReq: &transfer.RawReq{
			Req: &transfer.RawReq_OpenReq{
				OpenReq: &transfer.OpenReq{
					Path:     &path,
					Mode:     &mode,
					Snapshot: &snapshot,
				},
			},
		},
		ReqId:    &reqId,
		UserName: &user,
		UserKey:  &key,
	}

	return proto.Marshal(req)
}

func parseOpenResBin(bin []byte) (error, string) {
	res := &transfer.Res{}
	if err := proto.Unmarshal(bin, res); err != nil {
		return err, ""
	}
	var openRes *transfer.OpenRes
	switch x := res.Res.(type) {
	case *transfer.Res_OpenRes:
		openRes = x.OpenRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil), ""
	}
	switch x := openRes.Res.(type) {
	case *transfer.OpenRes_Error:
		if x.Error.AdviceNode != nil {
			return nil, *x.Error.AdviceNode
		}
		if x.Error.HashNodes != nil {
			return nil, *x.Error.HashNodes.HashNodeList[0].Node
		}
		return myutil.NewError(myutil.RequestReturnsError, nil), ""
	}
	return nil, ""
}

func sendOpenReq(url string, req []byte) ([]byte, net.Conn, error) {
	conn, err := net.Dial("tcp", url)
	if err != nil {
		info := fmt.Sprintf("can not connect to socket %s", url)
		return nil,conn, myutil.NewError1(myutil.SocketError, info, err)
	}

	err = socket_util.SendReq(conn, req)
	if err != nil {
		return nil, conn,  err
	}
	res, err := socket_util.ReadRes(conn)
	if err != nil {
		return nil, conn,  err
	}
	return res, conn, nil
}