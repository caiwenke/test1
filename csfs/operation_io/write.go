package operation_io

import (
	"csfs/pb/transfer"
	"log"
	"csfs/socket_util"
	"csfs/myutil"
)

func newWriteReq(reqId uint64, data []byte, off uint64) ([]byte, error) {
	rawReq := &transfer.RawReq{
		Req: &transfer.RawReq_WriteObjReq{
			WriteObjReq: &transfer.WriteObjReq{
				Data:   data,
				Offset: &off,
			},
		},
	}
	return newReqBin(rawReq, reqId)
}

//write requests don't have to be put in raqQueue
func (f *FileInfo) Write(data []byte, off int64) (uint32, error) {
	f.Lock.Lock()
	defer f.Lock.Unlock()
	log.Println("[write] ", f.path, " offset: ", off, " length: ", len(data))
	if f.err != nil {
		return 0, f.err
	}
	req, err := newWriteReq(f.NextReqId(), data, uint64(off))
	if err != nil {
		return 0, err
	}
	err = socket_util.SendReq(f.conn, req)
	if err != nil {
		f.err = err
		return 0, err
	}
	//update file Attr
	newSize := uint64(off) + uint64(len(data))
	if newSize > f.Attr.Size {
		blocks := newSize / 512
		if newSize%512 != 0 {
			blocks++
		}
		f.Attr.Size = newSize
		f.Attr.Blocks = blocks
	}
	//f.Attr.Mtime = uint64(time.Now().UTC().Unix())
	f.Attr.Mtime = myutil.ServerTime()

	return uint32(len(data)), nil
}
