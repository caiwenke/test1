package operation_io

import (
	"csfs/myutil"
	"csfs/pb/transfer"
	"github.com/golang/protobuf/proto"
	"errors"
	"fmt"
	"log"
	"csfs/socket_util"
)

func newReleaseReqBin(reqId uint64, path string) ([]byte, error) {
	user := myutil.GLOBAL.Username()
	key := myutil.GLOBAL.UserKey()
	req := &transfer.Req{
		RawReq: &transfer.RawReq{
			Req: &transfer.RawReq_ReleaseReq{
				ReleaseReq: &transfer.ReleaseReq{

				},
			},
		},
		ReqId:    &reqId,
		UserName: &user,
		UserKey:  &key,
	}
	return proto.Marshal(req)
}

func (f *FileInfo) ReleaseReq() (chan *message, error) {
	FIleMapLock.Lock()
	defer FIleMapLock.Unlock()
	f1, exist := FileMap[f.path]
	if !exist {
		err := errors.New(fmt.Sprintf("%s is not found in filemap", f.path))
		return nil, err
	}
	f1.Lock.Lock()
	defer f1.Lock.Unlock()
	f1.rc--
	if f1.rc == 0 {
		log.Println("remove ", f1.path, " from filemap")
		delete(FileMap, f1.path)
		reqId := f1.NextReqId()
		bin, err := newReleaseReqBin(reqId, f.path)
		if err != nil {
			return nil, err
		}
		socket_util.SendReq(f1.conn, bin)
		ch := make(chan *message)
		f1.reqQueue[reqId] = ch
		return ch, nil
	} else {
		return nil, nil
	}

}

func (f *FileInfo) ReleaseWait(ch chan *message) error {
	message, err := reciveOrTimeout(ch)
	if err != nil {
		return err
	}
	if message.err != nil {
		return message.err
	}
	res := message.res
	var releaseRes *transfer.ReleaseRes
	switch x := res.Res.(type) {
	case *transfer.Res_ReleaseRes:
		releaseRes = x.ReleaseRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch x := releaseRes.Res.(type) {

	case *transfer.ReleaseRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}
