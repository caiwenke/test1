package operation_io

import (
	"csfs/pb/transfer"
	"log"
	"csfs/myutil"
	"csfs/socket_util"
)

func newFlushReq(reqId uint64) ([]byte, error) {
	rawReq := &transfer.RawReq{
		Req: &transfer.RawReq_SyncReq{
			SyncReq: &transfer.SyncReq{

			},
		},
	}
	return newReqBin(rawReq, reqId)
}

func (f *FileInfo) FlushReq() (chan *message, error) {
	f.Lock.Lock()
	defer f.Lock.Unlock()
	if f.err != nil {
		return nil, f.err
	}
	log.Println("[flush] start ", f.path)
	reqId := f.NextReqId()
	req, err := newFlushReq(reqId)
	if err != nil {
		f.err = err
		return nil, err
	}
	err = socket_util.SendReq(f.conn, req)
	if err != nil {
		f.err = err
		return nil, err
	}
	ch := make(chan *message)
	f.reqQueue[reqId] = ch
	return ch, nil
}

func (f *FileInfo) FlushWait(ch chan *message) error {

	//todo no need for readonly files to flush

	message, err := reciveOrTimeout(ch)
	if err != nil {
		return err
	}
	if message.err != nil {
		return message.err
	}
	res := message.res
	var syncRes *transfer.SyncRes
	switch x := res.Res.(type) {
	case *transfer.Res_SyncRes:
		syncRes = x.SyncRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := syncRes.Res.(type) {

	case *transfer.SyncRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	return nil
}
