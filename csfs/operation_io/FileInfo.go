package operation_io

import (
	"sync"
	"github.com/hanwen/go-fuse/fuse"
	"net"
	"log"
	"csfs/socket_util"
	"csfs/operation"
	"csfs/myutil"
	"csfs/pb/transfer"
	"github.com/golang/protobuf/proto"
)

var FileMap = make(map[string]*FileInfo)
var FIleMapLock = sync.Mutex{}
var AdviseNode string

type message struct {
	res *transfer.Res
	err error
}

type FileInfo struct {
	Lock     *sync.Mutex
	Attr     *fuse.Attr
	rc       uint64
	reqId    uint64
	path     string
	conn     net.Conn
	err      error
	reqQueue map[uint64]chan *message
}

func newFileInfo(attr *fuse.Attr, rc uint64, path string, conn net.Conn) *FileInfo {
	f := &FileInfo{
		Lock:     &sync.Mutex{},
		Attr:     attr,
		rc:       rc,
		reqId:    1,
		path:     path,
		conn:     conn,
		err:      nil,
		reqQueue: make(map[uint64]chan *message),
	}
	return f
}

//func (f *FileInfo) ReadSync(dest []byte, off int64) (fuse.ReadResult, fuse.Status) {
//
//}
//
//func (f *FileInfo) ReadAsync(dest []byte, off int64) {
//	f.Lock.Lock()
//	defer f.Lock.Unlock()
//
//}
func (f *FileInfo) NextReqId() uint64 {
	f.reqId++
	return f.reqId
}

//func (f *FileInfo) Release() {
//	FIleMapLock.Lock()
//	defer FIleMapLock.Unlock()
//	log.Println("release: ", f.path)
//	f1, exist := FileMap[f.path]
//	if ! exist {
//		log.Println("error: ", f.path, " is not found in filemap")
//		return
//	}
//	f1.Lock.Lock()
//	defer f1.Lock.Unlock()
//	f1.rc--
//	log.Println("reference count of", f.path, ": ", f1.rc)
//	if f1.rc == 0 {
//		log.Println("remove ", f1.path, " from filemap")
//		delete(FileMap, f1.path)
//		reqId := f1.NextReqId()
//		bin, err := newReleaseReqBin(reqId, f.path)
//		log.Println("release reqID", reqId)
//		if err != nil {
//			log.Println("error: ", err)
//		} else {
//			socket_util.SendReq(f1.conn, bin)
//			f1.conn.Close()
//		}
//	}
//}

func Open(path string, flags uint32) (*File, error) {
	FIleMapLock.Lock()
	defer FIleMapLock.Unlock()
	log.Println("[open]", path)
	f, exist := FileMap[path]
	if exist {
		f.Lock.Lock()
		defer f.Lock.Unlock()
		f.rc ++
		log.Println("found file", path, "in filemap, reference count:", f.rc)
		return newFile(f), nil
	}
	req, err := newOpenReqBin(0, path)
	if err != nil {
		return nil, err
	}

	csStat, err := operation.GetAttr(&path)

	if err != nil {
		return nil, err
	}
	nLink := uint32(1)
	if csStat.StNlink != nil {
		nLink = uint32(*csStat.StNlink)
	}
	size := *csStat.StSize
	blocks := size / 512
	if size%512 != 0 {
		blocks++
	}
	attr := &fuse.Attr{
		Mode:    myutil.CsMode2AttrMode(csStat.StMode),
		Nlink:   nLink,
		Size:    size,
		Blocks:  blocks,
		Blksize: myutil.GLOBAL.BlockSize(),
		Atime:   *csStat.StAtim,
		Ctime:   *csStat.StCtim,
		Mtime:   *csStat.StMtim,
		Ino:     *csStat.StIno,
	}

	//todo retry
	url, err := myutil.GLOBAL.TransferNode(attr.Ino)
	if err != nil {
		return nil, err
	}

	conn, url, err := nodeIsOk(url, req)

	file := newFileInfo(attr, 1, path, conn)

	FileMap[path] = file
	go receiveLoop(file)
	return newFile(file), nil
}

func nodeIsOk(url string , req []byte) (net.Conn, string, error){
	for {
		res, conn, err := sendOpenReq(url, req)
		if err != nil {
			return  conn, "", err
		}
		err ,test := parseOpenResBin(res)
		if err != nil {
			return conn, "", err
		}
		if test != ""{
			url, err = myutil.GLOBAL.FindAnotherNode(test)
			if err := conn.Close() ; err != nil {
				return conn, "", err
			}
			continue
		}
		return conn,url ,err
	}
}
func receiveLoop(f *FileInfo) {
	var errTop error
	log.Println("receiveLoop")
	for {
		resBin, err := socket_util.ReadRes(f.conn)
		if err != nil {
			f.Lock.Lock()
			f.err = err
			f.Lock.Unlock()
			errTop = err
			break
		}

		res := &transfer.Res{}
		if err := proto.Unmarshal(resBin, res); err != nil {
			f.Lock.Lock()
			f.err = err
			f.Lock.Unlock()
			errTop = err
			break
		}

		switch x := res.Res.(type) {
		case *transfer.Res_WriteObjRes:
			switch y := x.WriteObjRes.Res.(type) {
			case *transfer.WriteObjRes_Error:
				err := myutil.NewError1(myutil.RequestReturnsError, *y.Error.Error, nil)
				f.Lock.Lock()
				f.err = err
				f.Lock.Unlock()
				errTop = err
				break
			case *transfer.WriteObjRes_Success:
				continue
			}
		}

		f.Lock.Lock()
		ch, exist := f.reqQueue[*res.ReqId]
		if exist {
			delete(f.reqQueue, *res.ReqId)
		}
		f.Lock.Unlock()

		if exist {
			ch <- &message{
				res: res,
			}
		} else {
			err := myutil.NewError1(myutil.RequestReturnsError, "can not found reqID from reqQueue", nil)
			f.Lock.Lock()
			f.err = err
			f.Lock.Unlock()
			errTop = err
			break
		}

	}
	//todo clear reqQueue
	// if errTop == nil, reqQueue must be empty
	if errTop != nil {
		f.Lock.Lock()
		defer f.Lock.Unlock()
		for reqId, ch := range f.reqQueue {
			msg := &message{
				err: errTop,
			}
			ch <- msg
			log.Println("send msg to reqId: ", reqId)
		}
	}
}
