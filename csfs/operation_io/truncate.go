package operation_io

import (
	"csfs/pb/transfer"
	"log"
	"csfs/socket_util"
	"csfs/myutil"
)

func newTruncateReq(reqId uint64, path *string, size uint64) ([]byte, error) {
	rawReq := &transfer.RawReq{
		Req: &transfer.RawReq_TruncateReq{
			TruncateReq: &transfer.TruncateReq{
				Size: &size,
				Path: path,
			},
		},
	}
	return newReqBin(rawReq, reqId)
}

func (f *FileInfo) TruncateReq(size uint64) (chan *message, error) {
	f.Lock.Lock()
	defer f.Lock.Unlock()
	if f.err != nil {
		return nil, f.err
	}
	reqId := f.NextReqId()
	log.Println("[truncate] start", f.path, "reqID", reqId)
	req, err := newTruncateReq(reqId, &f.path, size)
	if err != nil {
		f.err = err
		return nil, err
	}
	err = socket_util.SendReq(f.conn, req)
	if err != nil {
		f.err = err
		return nil, err
	}
	ch := make(chan *message)
	f.reqQueue[reqId] = ch

	newSize := size
	blocks := newSize / 512
	if newSize%512 != 0 {
		blocks++
	}
	f.Attr.Size = newSize
	f.Attr.Blocks = blocks

	//f.Attr.Mtime = uint64(time.Now().UTC().Unix())
	f.Attr.Mtime = myutil.ServerTime()
	return ch, nil
}

func (f *FileInfo) TruncateWait(ch chan *message) error {
	message, err := reciveOrTimeout(ch)
	if err != nil {
		return err
	}
	if message.err != nil {
		return message.err
	}
	res := message.res
	var truncateRes *transfer.TruncateRes
	switch x := res.Res.(type) {
	case *transfer.Res_TruncateRes:
		truncateRes = x.TruncateRes
	default:
		return myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	switch  x := truncateRes.Res.(type) {

	case *transfer.TruncateRes_Error:
		return myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	log.Println("[truncate] finish, reqID", *res.ReqId)
	return nil
}
