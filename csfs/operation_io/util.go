package operation_io

import (
	"csfs/myutil"
	"csfs/pb/transfer"
	"github.com/golang/protobuf/proto"
	"time"
)

func newReqBin(rawReq *transfer.RawReq, reqId uint64) ([]byte, error) {
	user := myutil.GLOBAL.Username()
	key := myutil.GLOBAL.UserKey()
	req := &transfer.Req{
		UserKey:  &key,
		UserName: &user,
		RawReq:   rawReq,
		ReqId:    &reqId,
	}
	return proto.Marshal(req)
}

func reciveOrTimeout(ch chan *message) (*message, error) {
	timeout := myutil.GLOBAL.ReceiveTimeout()
	//timeoutSec :=30
	select {
	case msg := <-ch:
		return msg, nil
	case <-time.After(timeout):
		info := "timeout to receive response from socket"
		return nil, myutil.NewError1(myutil.ReceiveTimeout, info, nil)
	}
}
