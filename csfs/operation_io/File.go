package operation_io

import (
	"github.com/hanwen/go-fuse/fuse/nodefs"
	"github.com/hanwen/go-fuse/fuse"
	"time"
	"log"
	"csfs/myutil"
	"fmt"
)

type File struct {
	inner *FileInfo
}

func newFile(inner *FileInfo) *File {
	f := File{
		inner: inner,
	}
	return &f
}

func (f *File) Release() {
	//go func() {
	time.Sleep(1 * time.Millisecond)
	ch, err := f.inner.ReleaseReq()
	if err != nil {
		log.Println("[release]", f.inner.path, "error:", err)
		return
	}
	if ch != nil {
		err := f.inner.ReleaseWait(ch)
		if err != nil {
			log.Println("[release]", f.inner.path, "error:", err)
		}
		f.inner.conn.Close()
	}
	//}()
}

func (f *File) SetInode(inode *nodefs.Inode) {
}

func (f *File) String() string {
	if f == nil {
		return "CsFile"
	}
	return fmt.Sprintf("CsFile %s", f.inner.path)
}

func (f *File) InnerFile() nodefs.File {
	return nil
}

func (f *File) Read(buf []byte, off int64) (fuse.ReadResult, fuse.Status) {
	ch, err := f.inner.ReadReq(uint64(off), uint32(len(buf)))
	if err != nil {
		log.Println("[read] ", f.inner.path, " error ", err)
		return nil, myutil.ToStatus(err)
	}
	data, err := f.inner.ReadWait(ch)
	if err != nil {
		log.Println("[read] ", f.inner.path, " error ", err)
		return nil, myutil.ToStatus(err)
	}
	log.Println("[read]", f.inner.path, " offset:", off, " length: ", len(data))
	return fuse.ReadResultData(data), fuse.OK
}

func (f *File) Write(data []byte, off int64) (uint32, fuse.Status) {
	res, err := f.inner.Write(data, off)
	if err != nil {
		log.Println("[write] ", f.inner.path, " error ", err)
		return 0, myutil.ToStatus(err)
	}
	return res, fuse.OK
}

func (f *File) Flock(flags int) fuse.Status {
	return fuse.ENOSYS
}

func (f *File) Flush() fuse.Status {
	ch, err := f.inner.FlushReq()
	if err != nil {
		log.Println("[flush]", f.inner.path, "error", err)
		return myutil.ToStatus(err)
	}
	err = f.inner.FlushWait(ch)
	if err != nil {
		log.Println("[flush]", f.inner.path, "error", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}

func (f *File) Fsync(flags int) (code fuse.Status) {
	if f == nil {
		return fuse.ENOSYS
	}
	//  Fsync == Flush ??? todo
	return f.Flush()
}
func (f *File) Truncate(size uint64) fuse.Status {
	log.Println("[truncate]", f.inner.path, "size:", size)
	ch, err := f.inner.TruncateReq(size)
	if err != nil {
		log.Println("[truncate]", f.inner.path, "erro", err)
		return myutil.ToStatus(err)
	}
	err = f.inner.TruncateWait(ch)
	if err != nil {
		log.Println("[truncate]", f.inner.path, " error ", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}
func (f *File) GetAttr(old *fuse.Attr) fuse.Status {
	if old == nil || f == nil {
		return fuse.ENOSYS
	}
	f.inner.Lock.Lock()
	*old = *f.inner.Attr
	defer f.inner.Lock.Unlock()
	log.Println("[getattr]", f.inner.path, "size: ", old.Size)
	//log.Println("getattr", fmt.Sprintf("%#v", *old))
	return fuse.OK
}
func (f *File) Utimens(atime *time.Time, mtime *time.Time) fuse.Status {
	return fuse.ENOSYS
}
func (f *File) Chown(uid uint32, gid uint32) fuse.Status {
	return fuse.ENOSYS
}

func (f *File) Chmod(perms uint32) fuse.Status {
	return fuse.ENOSYS
}

func (f *File) Allocate(off uint64, size uint64, mode uint32) (code fuse.Status) {
	return fuse.ENOSYS
}
