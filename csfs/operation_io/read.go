package operation_io

import (
	"csfs/pb/transfer"
	"log"
	"csfs/socket_util"
	"csfs/myutil"
	"time"
)

func newReadReq(reqId uint64, off uint64, length uint32) ([]byte, error) {
	rawReq := &transfer.RawReq{
		Req: &transfer.RawReq_ReadObjReq{
			ReadObjReq: &transfer.ReadObjReq{
				Offset: &off,
				Length: &length,
			},
		},
	}
	return newReqBin(rawReq, reqId)
}

func (f *FileInfo) ReadReq(off uint64, length uint32) (chan *message, error) {
	f.Lock.Lock()
	defer f.Lock.Unlock()
	reqId := f.NextReqId()
	log.Println("[read] reqId: ", reqId, " start ", f.path, " offset: ", off, " length: ", length)
	if f.err != nil {
		return nil, f.err
	}

	req, err := newReadReq(reqId, off, length)
	if err != nil {
		f.err = err
		return nil, err
	}
	err = socket_util.SendReq(f.conn, req)
	if err != nil {
		f.err = err
		return nil, err
	}
	f.Attr.Atime = uint64(time.Now().UTC().Unix())
	ch := make(chan *message)
	f.reqQueue[reqId] = ch
	return ch, nil
}

func (f *FileInfo) ReadWait(ch chan *message) ([]byte, error) {
	message, err := reciveOrTimeout(ch)
	if err != nil {
		return nil, err
	}

	if message.err != nil {
		return nil, message.err
	}
	res := message.res

	var readRes *transfer.ReadObjRes
	switch x := res.Res.(type) {
	case *transfer.Res_ReadObjRes:
		readRes = x.ReadObjRes
	default:
		return nil, myutil.NewError(myutil.ResponseTypeMissMatch, nil)
	}

	var data []byte
	switch  x := readRes.Res.(type) {
	case *transfer.ReadObjRes_ReadPayload:
		data = x.ReadPayload.Data
	case *transfer.ReadObjRes_Error:
		return nil, myutil.NewError1(myutil.RequestReturnsError, *x.Error.Error, nil)
	}
	log.Println("[read] receive response, id: ", *res.ReqId, " length: ", len(data))
	return data, nil
	//return fuse.ReadResultData(data),fuse.OK
}
