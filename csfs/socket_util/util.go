package socket_util

import (
	"net"
	"encoding/binary"
	"io"
	"csfs/myutil"
)

func SendReq(conn net.Conn, data []byte) error {
	header := make([]byte, 4)
	binary.BigEndian.PutUint32(header, uint32(len(data)))

	if _, err := conn.Write(header); err != nil {
		return err
	}

	if _, err := conn.Write(data); err != nil {
		return err
	}
	return nil
}

func ReadRes(conn net.Conn) ([]byte, error) {
	header := make([]byte, 4)
	if _, err := io.ReadFull(conn, header); err != nil {
		return nil, myutil.NewError1(myutil.SocketError, "can not read length header from socket", err)
	}
	length := binary.BigEndian.Uint32(header)
	data := make([]byte, length)
	if _, err := io.ReadFull(conn, data); err != nil {
		return nil, myutil.NewError1(myutil.SocketError, "can not read response body from socket", err)
	}
	return data, nil
}
