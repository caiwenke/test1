package main

import (
	"github.com/hanwen/go-fuse/fuse"
	"log"
	"path"
	"csfs/myutil"
	"csfs/operation"
	"time"
	"github.com/hanwen/go-fuse/fuse/nodefs"
	"github.com/hanwen/go-fuse/fuse/pathfs"
	"csfs/operation_io"
	"csfs/pb/mds"
)

type HelloFs struct {
	//pathfs.FileSystem
}

func (me *HelloFs) String() string {
	return "csfs"
}

// If called, provide debug output through the log package.
func (me *HelloFs) SetDebug(debug bool) {}

// Attributes.  This function is the main entry point, through
// which FUSE discovers which files and directories exist.
//
// If the filesystem wants to implement hard-links, it should
// return consistent non-zero FileInfo.Ino data.  Using
// hardlinks incurs a performance hit.
func (me *HelloFs) GetAttr(name string, context *fuse.Context) (*fuse.Attr, fuse.Status) {
	p := path.Join("/", name)
	operation_io.FIleMapLock.Lock()
	f1, exist := operation_io.FileMap[p]
	if exist {
		f1.Lock.Lock()
		attr := fuse.Attr{}
		attr = *f1.Attr
		f1.Lock.Unlock()
		operation_io.FIleMapLock.Unlock()
		log.Println("getattr", p, "size:", attr.Size)
		return &attr, fuse.OK
	}
	operation_io.FIleMapLock.Unlock()

	res, err := operation.GetAttr(&p)
	if err != nil {
		log.Println("[getattr]", p, "error: ", err)
		return nil, myutil.ToStatus(err)
	}
	blocks := *res.StSize / 512
	if *res.StSize%512 != 0 {
		blocks ++
	}
	attr := &fuse.Attr{
		Mode:   myutil.CsMode2AttrMode(res.StMode),
		Size:   *res.StSize,
		Blocks: blocks,
		Atime:  *res.StAtim,
		Ctime:  *res.StCtim,
		Mtime:  *res.StMtim,
		Ino:    *res.StIno,
	}
	log.Println("getattr", p, "size:", attr.Size)
	return attr, fuse.OK
}

// These should update the file's ctime too.
func (me *HelloFs) Chmod(name string, mode uint32, context *fuse.Context) (code fuse.Status) {
	p := path.Join("/", name)
	log.Println("chmod", p)
	csMode := myutil.AttrMode2CsMode(mode)
	err := operation.Chmod(&p, csMode)
	if err != nil {
		log.Println("[chmod] error", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}

func (me *HelloFs) Chown(name string, uid uint32, gid uint32, context *fuse.Context) (code fuse.Status) {
	//return fuse.ENOSYS  //todo
	return fuse.OK
}
func (me *HelloFs) Utimens(name string, Atime *time.Time, Mtime *time.Time, context *fuse.Context) (code fuse.Status) {
	p := path.Join("/", name)
	log.Println("utimens", p)
	err := operation.Utimens(&p, uint64(Atime.Unix()), uint64(Mtime.Unix()))
	if err != nil {
		log.Println("[utimens] error", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}

//todo test
func (me *HelloFs) Truncate(name string, size uint64, context *fuse.Context) (code fuse.Status) {
	log.Println("[fs.truncate] open file", name)
	file, state := me.Open(name, 0, context)
	if state != fuse.OK {
		return state
	}
	log.Println("[fs.truncate] file", name)
	state = file.Truncate(size)
	log.Println("[fs.truncate] release file", name)
	file.Release()
	return state
	//p := path.Join("/", name)
	//log.Println("truncate", p)
	//err := operation.Truncate(&p, size)
	//if err != nil {
	//	log.Println("[truncate] error", err)
	//	return myutil.ToStatus(err)
	//}
	//return fuse.OK
}

func (me *HelloFs) Access(name string, mode uint32, context *fuse.Context) (code fuse.Status) {
	log.Println("access: ", name)
	return fuse.ENOSYS
}

// Tree structure
func (me *HelloFs) Link(oldName string, newName string, context *fuse.Context) (code fuse.Status) {
	old := path.Join("/", oldName)
	new := path.Join("/", newName)
	log.Println("link", old, " -> ", new)
	err := operation.Link(&old, &new)
	if err != nil {
		log.Println("[link] error", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}
func (me *HelloFs) Mkdir(name string, mode uint32, context *fuse.Context) fuse.Status {
	p := path.Join("/", name)
	log.Println("mkdir", p)
	err := operation.Mkdir(&p)
	if err != nil {
		log.Println("[mkdir] error", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}
func (me *HelloFs) Mknod(name string, mode uint32, dev uint32, context *fuse.Context) fuse.Status {
	return fuse.ENOSYS
}

// todo fix rename file that exists
func (me *HelloFs) Rename(oldName string, newName string, context *fuse.Context) (code fuse.Status) {
	old := path.Join("/", oldName)
	new := path.Join("/", newName)
	log.Println("rename", old, " -> ", new)
	err := operation.Rename(&old, &new)
	if err != nil {
		log.Println("[rename] error", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}

func (me *HelloFs) Rmdir(name string, context *fuse.Context) (code fuse.Status) {
	p := path.Join("/", name)
	log.Println("rmdir", p)
	err := operation.Rmdir(&p)
	if err != nil {
		log.Println("[rmdir] error", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}

func (me *HelloFs) Unlink(name string, context *fuse.Context) (code fuse.Status) {
	p := path.Join("/", name)
	log.Println("unlink", p)
	err := operation.Unlink(&p)
	if err != nil {
		log.Println("[unlink] error", err)
		return myutil.ToStatus(err)
	}
	return fuse.OK
}

// Extended attributes.
func (me *HelloFs) GetXAttr(name string, attribute string, context *fuse.Context) (data []byte, code fuse.Status) {
	return nil, fuse.ENOSYS
}
func (me *HelloFs) ListXAttr(name string, context *fuse.Context) (attributes []string, code fuse.Status) {
	return nil, fuse.ENOSYS
}
func (me *HelloFs) RemoveXAttr(name string, attr string, context *fuse.Context) fuse.Status {
	return fuse.ENOSYS
}
func (me *HelloFs) SetXAttr(name string, attr string, data []byte, flags int, context *fuse.Context) fuse.Status {
	return fuse.ENOSYS
}

// Called after mount.
func (me *HelloFs) OnMount(nodeFs *pathfs.PathNodeFs) {}
func (me *HelloFs) OnUnmount()                        {}

// File handling.  If opening for writing, the file's mtime
// should be updated too.

// todo fix
func (me *HelloFs) Open(name string, flags uint32, context *fuse.Context) (file nodefs.File, code fuse.Status) {
	p := path.Join("/", name)
	log.Println("[open]", p)

	for i := 0; i < 2; i++ {
		file, err := operation_io.Open(p, flags)
		if err != nil {
			log.Println("[open] error: ", err, "retry")
			myutil.UpdateGlobalState()
			continue
		}
		return file, fuse.OK
	}

	file, err := operation_io.Open(p, flags)
	if err != nil {
		log.Println("[open] error: ", err)
		return nil, myutil.ToStatus(err)
	}
	return file, fuse.OK
}

//todo
func (me *HelloFs) Create(name string, flags uint32, mode uint32, context *fuse.Context) (file nodefs.File, code fuse.Status) {
	p := path.Join("/", name)
	log.Println("create", p)
	false_ := false
	true_ := true
	accessMode := mds.AccessMode_O_RDWR
	err := operation.Create(&p, &mds.CreationFlag{O_EXCL: &false_, O_CREAT: &true_}, &accessMode, myutil.GLOBAL.SessionID())
	if err != nil {
		log.Println("[create] error", err)
		return nil, myutil.ToStatus(err)
	}
	return me.Open(name, flags, context)
}

// Directory handling

func (me *HelloFs) OpenDir(name string, context *fuse.Context) (c []fuse.DirEntry, code fuse.Status) {
	p := path.Join("/", name)
	log.Println("opendir: ", p)
	res, err := operation.OpenDir(p)
	if err != nil {
		log.Println("[opendir] error", err)
		return nil, myutil.ToStatus(err)
	}
	fileList := res.PageList
	c = make([]fuse.DirEntry, len(fileList))
	for i, f := range fileList {
		c[i].Name = *f.Filename
		switch    *f.Stat.StMode.Type {
		case mds.Type_dir:
			c[i].Mode = fuse.S_IFDIR
		case mds.Type_file:
			c[i].Mode = fuse.S_IFREG
		}
	}
	return c, fuse.OK
}

// Symlinks.
func (me *HelloFs) Symlink(value string, linkName string, context *fuse.Context) (code fuse.Status) {
	return fuse.ENOSYS
}
func (me *HelloFs) Readlink(name string, context *fuse.Context) (string, fuse.Status) {
	log.Println("readlink", name)
	return "", fuse.ENOSYS
}

func (me *HelloFs) StatFs(name string) *fuse.StatfsOut {
	p := path.Join("/", name)
	log.Println("statfs", p)
	statfs, err := operation.StatFs(&p)
	if err != nil {
		log.Println("[statfs] error", err)
		return nil
	}
	res := &fuse.StatfsOut{
		Bavail:  *statfs.FBavail,
		Bfree:   *statfs.FBfree,
		Blocks:  *statfs.FBlocks,
		Bsize:   uint32(*statfs.FBsize),
		Ffree:   *statfs.FFfree,
		Files:   *statfs.FFiles,
		Frsize:  uint32(*statfs.FFrsize),
		NameLen: uint32(*statfs.FNamemax),
	}
	return res
}
