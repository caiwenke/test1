package hashring

import (
	"csfs/pb/mds"
	"runtime"
)

type TransferRing struct {
	inner *Ring
	m     map[string]*mds.TransferServer
}

func finalizer(r *TransferRing) {
	if r != nil {
		r.free()
	}
}

func (r *TransferRing) free() {
	//log.Println("[hashring] free", r)
	if r.inner != nil {
		r.inner.Free()
	}
}

func NewTransferRing(numReplicas int, list []*mds.TransferServer) *TransferRing {
	res := &TransferRing{
		inner: New(numReplicas, SHA1),
		m:     make(map[string]*mds.TransferServer),
	}

	for _, transfer := range list {
		res.inner.Add([]byte(*transfer.Node))
		res.m[*transfer.Node] = transfer
	}
	runtime.SetFinalizer(res, finalizer)
	//log.Println("[hashring] new", res)
	return res
}

func (r *TransferRing)Getm() map[string]*mds.TransferServer {
	return r.m
}

func (r *TransferRing) FindNode(key []byte, num int) []*mds.TransferServer {
	nodes := r.inner.FindNodes(key, num)
	res := make([]*mds.TransferServer, 0)
	for _, node := range nodes {
		t := r.m[string(node)]
		res = append(res, t)
	}
	return res
}
